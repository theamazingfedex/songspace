<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="user" tagdir="/WEB-INF/tags/user" %>
<!DOCTYPE html>

<html>
  <head>
    	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    	<link rel="stylesheet" type="text/css" href="style.css" />
    	<title>Group Lab | Log In</title>
  </head>
  <body id="LogIn" class="Default">
  
  <div id="Page">
    <article>
      <h2>Log On</h2>
      <table>
      	<tr>
      		<td>
				<user:LogOn/>      		
      		</td>
      		<td>
      			<a href="register.jsp"></a>
      		</td>
      	</tr>
      </table>
    </article>
  </div>
       
  </body>
</html>