<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@taglib prefix="template" tagdir="/WEB-INF/tags" %>
<%@taglib prefix="user" tagdir="/WEB-INF/tags/user" %>
<!DOCTYPE html>

<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <link rel="stylesheet" type="text/css" href="style.css" />
  <title>GlassFish JSP Page</title>
</head>
<body>
	<template:Header />
	<template:Nav />
	<article>
		<h2>Account</h2>
		
		${curUser.getDisplayname()}
		${uname}
		
		<p>${displayname}</p>
		
		<user:UserFriends />
		<user:UserPlayLists />
		
	</article>
</body>
</html>

<!-- Display Name -->
<!-- Email -->

<!-- 

View the account overview.[1]
i. A user may edit the account.[1]
ii. A user may upload a song.[1]
1. A song consists of:
a. Id
b. Name
c. Description
d. Artist
e. DateAdded
f. Ranking[2]
g. Playlists
h. User
i. FileLocation
A user may edit or remove songs[1]
iii.
A user may create playlists[1]
iv.
1. A Playlist consists of:
a. Id
b. Name
c. Description
d. DateAdded
e. Songs
f. Comments
g. User
v. A user may edit or remove playlists[1]
A user may create Posts.[1]
vi.
1. A post consists of:
a. Id
b. Content
c. Comments
d. User
e. DatePosted
vii. A user may edit or remove posts.[1]
viii. A user may view friends.[1]
1. A user may remove friends.[1]
 -->