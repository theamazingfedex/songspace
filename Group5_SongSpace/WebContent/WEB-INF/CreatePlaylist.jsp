<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page import="entities.Song" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>SongSpace</title>
</head>
<body>
	<h2>Create Playlist</h2>
	<form method="post" action="PlaylistServlet">
		<input type="hidden" name="crudType" value="create"/>
		<label>Playlist Name:</label><input type="text" name="playlistName"/><br>
		<label>Songs:</label>
		<select multiple="multiple" name="songslist">
			<option value="1">Test Value(1)</option>
			<c:forEach var="s" items="${songs}">
				<option value="${s.getId()}">${s.getName() }</option>
			</c:forEach>
		</select><br>
		<label>Description</label><input type="text" name="description"/><br>
		<input type="submit" value="Create"/>
	</form>
</body>
</html>