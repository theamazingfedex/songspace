<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>SongSpace</title>
</head>
<body>
	<h2>${song.getName() }</h2>
	<br><br>
	<p>${song.getArtist() }</p>
	<p>${song.getDescription() }</p>
	<audio controls="controls">
		<source src="${song.getSongFile() }" type="audio/mp3"></source>
	</audio>
</body>
</html>