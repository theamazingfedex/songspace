<%@ tag language="java" pageEncoding="ISO-8859-1"  %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<h3>Songs</h3>
<table border="1">
	<tr>
	  <th>Name</th>
	  <th>Artist</th>
	  <th>Date Added</th>
	</tr>
	<c:forEach var="song" items="${songs}">
		<tr>
			<td>${song.getName()}</td>
			<td>${song.getArtist()}</td>
			<td>${song.getAddedDate()}</td>
		</tr>
	</c:forEach>
</table>