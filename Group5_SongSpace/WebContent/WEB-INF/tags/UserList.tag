<%@ tag language="java" pageEncoding="ISO-8859-1"  %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<h3>Users</h3>
<table border="1" style="width:600px;">
	<tr>
	  <th>Id</th>
	  <th>Display Name</th>
	  <th>Email</th>
	</tr>
	<c:forEach var="user" items="${users}">
		<tr>
			<td>${user.getId()}</td>
			<td>${user.getDisplayname()}</td>
			<td>${user.getEmail()}</td>
		</tr>
	</c:forEach>
</table>