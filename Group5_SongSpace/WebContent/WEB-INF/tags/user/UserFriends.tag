<%@ tag language="java" pageEncoding="ISO-8859-1"  %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<h3>Friends</h3>
<table border="1">
	<tr>
	  <th>Id</th>
	</tr>
	<c:forEach var="user" items="${friends}">
		<tr>
			<td>${user.getId()}</td>
		</tr>
	</c:forEach>
</table>