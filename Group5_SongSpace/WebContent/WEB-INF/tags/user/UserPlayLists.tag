<%@ tag language="java" pageEncoding="ISO-8859-1"  %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<h3>Playlists</h3>
<table border="1">
	<tr>
	  <th>Id</th>
	</tr>
	<c:forEach var="playlist" items="${playlists}">
		<tr>
			<td>${playlist.getId()}</td>
		</tr>
	</c:forEach>
</table>