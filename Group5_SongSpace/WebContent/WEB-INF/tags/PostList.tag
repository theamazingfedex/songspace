<%@ tag language="java" pageEncoding="ISO-8859-1" import="entities.Post" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<h3>Posts</h3>
<table border="1">
	<tr>
	  <th>Title</th>
	</tr>
	<c:forEach var="post" items="${posts}">
		<tr>
			<td>${post.getTitle()}</td>
		</tr>
	</c:forEach>
</table>