<%@ tag language="java" pageEncoding="ISO-8859-1" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<c:if test="${settings.getCommentsEnabled() != 'Disabled'}">
	<form method="post" action="CommentServlet.do" id="Comment" onsubmit="return validateCommentForm()">
		<label>Comment:</label><br />
		<input type="hidden" name="crudType" value="create" />
		<input type="hidden" name="post" value="${post.getPostId()}" />
		<textarea name="comment" id="comment"></textarea><br />
		<input type="submit" value="Comment" />
	</form>
</c:if>