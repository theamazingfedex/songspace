<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="ranking" tagdir="/WEB-INF/tags/ranking" %>
<%@ page import="entities.Playlist" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>SongSpace</title>
</head>
<body>
	<h2>Playlists</h2>
	<a href="Index">Index</a>
	<table border="1">
		<tr>
			<th>Playlist Name</th>
			<th>Description</th>
		</tr>
		<c:forEach var="playlist" items="${playlists }">
		
			<tr>
				<td>${playlist.name }</td>
				<td>${playlist.description }</td>
    			<td>Current Ranking: <b>${playlist.ranking }</b>
    			<ranking:playlistRanking objectId="${playlist.id }"/></td>
				<td>
					<form method="get" action="PlaylistServlet">
						<input type="hidden" name="playlistID" value="${playlist.getId() }"/>
						<input type="hidden" name="crudType" value="update"/>
						<input type="submit" value="Edit"/>
					</form>
				</td>
				<td>
					<form method="get" action="PlaylistServlet">
						<input type="hidden" name="playlistID" value="${playlist.getId() }"/>
						<input type="hidden" name="crudType" value="delete"/>
						<input type="submit" value="Delete"/>
					</form>
				</td>				
				<td>
					<form method="get" action="PlaylistServlet">
						<input type="hidden" name="playlistID" value="${playlist.getId() }"/>
						<input type="hidden" name="crudType" value="view"/>
						<input type="submit" value="View"/>
					</form>
				</td>
			</tr>
		</c:forEach>
	</table>
</body>
</html>