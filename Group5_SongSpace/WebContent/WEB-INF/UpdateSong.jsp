<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>SongSpace</title>
</head>
<body>
	<h2>Update Song</h2>
	<form method="post" action="SongServlet">
		<input type="hidden" name="crudType" value="update"/>
		<input type="hidden" name="songID" value="${song.getID() }"/>
		<label>Song Name:</label><input type="text" name="songName" value="${song.getName() }"/>
		<label>Artist:</label><input type="text" name="artist" value="${song.getArtist() }"/>
		<label>Description:</label><input type="text" name="description" value="${song.getDescription }"/>
		<input type="submit" value="Update"/>
	</form>
</body>
</html>