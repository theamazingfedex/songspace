<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page import="entities.Song" %>
<%@ page import="entities.Playlist" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>SongSpace</title>
</head>
<body>
	<h2>Update Playlist</h2>
	<form method="post" action="PlaylistServlet">
		<input type="hidden" name="crudType" value="update"/>
		<input type="hidden" name="playlistID" value="${playlist.getId() }"/>
		<label>Playlist Name:</label><input type="text" name="playlistName" value="${playlist.getName() }"/><br>
		<label>Songs:</label>
		<c:forEach var="s" items="${playlist.getSongs() }">
			${s.getName() }
		</c:forEach>
		<select multiple="multiple" name="songslist">
			<c:forEach var="s" items="${songs}">
				<option value="${s.getId() }">${i.getName() }</option>
			</c:forEach>
		</select><br>
		<label>Description</label><input type="text" name="description" value="${p.getDescription() }"/><br>
		<label>Rating</label>
		<select name="curRating">
			<option>1</option>
			<option>2</option>
			<option>3</option>
			<option>4</option>
			<option>5</option>
			<option>6</option>
			<option>7</option>
			<option>8</option>
			<option>9</option>
			<option>10</option>
		</select>
		<input type="submit" value="Update"/>
	</form>
</body>
</html>