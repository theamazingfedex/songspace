<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
		    	<link rel="stylesheet" type="text/css" href="style.css" />
		<title>SongSpace</title>
		<script type="text/javascript">
		function checkEnableSubmit() {
		  if (document.getElementById("fname").value == "") // some logic to determine if it is ok to go
		    {document.getElementById("upload").disabled = true;
		    document.getElementById("upload").src = "upload_disabled.png";}
		  else // in case it was enabled and the user changed their mind
		    {document.getElementById("upload").disabled = false;
		    document.getElementById("upload").src = "upload.png";}
		}
</script>
	</head>
	<body>
		<h2>Upload Song</h2>
		<p>File types allowed: MP3</p>
		<form method="post" action="SongServlet" enctype="multipart/form-data">
			<input type="hidden" name="crudType" value="create"/>
			<label for="fname">Uploaded file:</label><input type="file" id="fname"  onchange="javascript:checkEnableSubmit()" /></br>
			<label for="songName">Song Name</label><input type="text" name="songName"/></br>
			<label>Description:</label><input type="text" name="description"/></br>
			<label>Artist:</label><input type="text" name="artist"/></br>
			<input type="submit" value="Upload Song"/>
		</form>
	</body>
</html>