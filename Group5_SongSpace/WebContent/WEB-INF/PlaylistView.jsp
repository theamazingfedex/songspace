<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>SongSpace</title>
</head>
<body>
	<h2>${playlist.getName() }</h2>
	<p>${playlist.getDescription() }</p>
	<table>
		<tr>
			<th>Song Name</th>
			<th>Artist</th>
		</tr>
		<c:forEach var="s" items="${playlist.getSongs() }" >
			<tr>
				<td>${s.getName() }</td>
				<td>${s.getArtist() }</td>
			</tr>			
			<tr>
				<td>
					<audio controls="controls">
						<source src="${s.getSongFile() }" type="${s.getDataType() }"></source>
					</audio>
				</td>
			</tr>
		</c:forEach>
	</table>
</body>
</html>