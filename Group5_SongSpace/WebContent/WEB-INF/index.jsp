<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="user" tagdir="/WEB-INF/tags/user" %>
<%@ taglib prefix="ranking" tagdir="/WEB-INF/tags/ranking" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    	               "http://www.w3.org/TR/html4/loose.dtd">

<html>
  <head>
    	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    	<title>Group 5 | SongSpace</title>
  </head>
  <body>
    <h1>Hello ${curUser.displayname}!</h1>
    <table border="1" cellpadding="10">
    	<tr>
    		<td>
    			LogOn:
    			<user:LogOn/>
   			</td>
   			
    		<td>
    			Register:
    			<user:Register/>
    		</td>
    		<c:if test="${curUser != null }">
	    		<td>
	    			LogOff:
	    			<user:LogOff/>
	    		</td>
    		</c:if>
    	</tr>
    	<tr>
    		<td>
			    <form method="get" action="SongServlet">
			    	<input type="hidden" name="crudType" value="create"/>
			    	<input type="submit" value="Upload Song"/>
			    </form>
    		</td>
    		<td>
			    <form method="get" action="SongServlet">
			    	<input type="hidden" name="crudType" value="preload"/>
			    	<input type="submit" value="View Songs"/>
			    </form>
    		</td>
    	</tr>
    	<tr>
    		<td>
			    <form method="get" action="PlaylistServlet">
			    	<input type="hidden" name="crudType" value="create"/>
			    	<input type="submit" value="Create Playlist"/>
			    </form>
    		</td>
    		<td>
			    <form method="get" action="PlaylistServlet">
			    	<input type="hidden" name="crudType" value="preload"/>
			    	<input type="submit" value="View Playlists"/>
			    </form>
    		</td>
    	</tr>
    	<tr><td><form method="get" action="AllUsers">
    				<input type="submit" value="View All Users"/>
    			</form>
    			</td></tr>
    			
    </table>
  </body>
</html> 
