<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="ranking" tagdir="/WEB-INF/tags/ranking" %>
<%@ page import="entities.Song" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>SongSpace</title>
</head>
<body>
	<h2>Songs</h2>
	<table border="1">
		<tr>
			<th>Song Name</th>
			<th>Artist</th>
		</tr>
		<c:forEach var="curSong" items="${songs }">
			<tr>
				<td>${curSong.getName() }</td>
				<td>${curSong.getArtist() }</td>
				<td>Ranking: <b>${curSong.ranking }</b>
				<ranking:songRanking objectId="${curSong.id }"/></td>
				<td>
					<form method="get" action="SongServlet">
						<input type="hidden" name="songID" value="${curSong.getId() }"/>
						<input type="hidden" name="crudType" value="update"/>
						<input type="submit" value="Edit"/>
					</form>
				</td>
				<td>
					<form method="get" action="SongServlet">
						<input type="hidden" name="songID" value="${curSong.getId() }"/>
						<input type="hidden" name="crudType" value="delete"/>
						<input type="submit" value="Delete"/>
					</form>
				</td>
				<td>
					<form method="get" action="SongServlet">
						<input type="hidden" name="songID" value="${curSong.getId() }"/>
						<input type="hidden" name="crudType" value="view"/>
						<input type="submit" value="View"/>
					</form>
				</td>
			</tr>
		</c:forEach>
	</table>
</body>
</html>