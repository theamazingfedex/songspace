<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>All Users | Admin Only</title>
</head>
<body>
	<table border="2" bordercolor="black">
		<tr>
    		<td>
    			<label>All Users:</label>
    			<c:forEach var="user" items="${allUsers }">
    			<tr>
    				<td>		
    				<a href="UpdateUser/${user.id }">${user.displayname }</a>
    				</td>
    				<td>
    				<form method="POST" action="DeleteUser">
    					<input type="hidden" name="delId" value="${user.id }"/>
    					<input type="submit" value="Delete" />
    				</form>
    				</td>
    			</tr>
    			</c:forEach>
    		</td>
   		</tr>
	</table>
</body>
</html>