package servlets;

import java.io.IOException;
import java.util.Date;
import java.sql.Timestamp;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import managers.CommentManager;
import managers.PostManager;
import managers.UserManager;

import entities.Comment;
import entities.Post;
import entities.User;

@WebServlet("/CommentServlet.do")

public class CommentServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	@EJB private CommentManager commentManager;
	@EJB private PostManager postManager;
	@EJB private UserManager userManager;

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String crud = request.getParameter("crudType");
		
		if(crud != null && !(crud.isEmpty())){
			if(crud.equalsIgnoreCase("create")){
				createComment(request);
			}else if(crud.equalsIgnoreCase("update")){
				updateComment(request);
			}else if(crud.equalsIgnoreCase("delete")){
				deleteComment(request);
			}
		}
		String postid = request.getParameter("post");
		System.out.println("HERP " + postid);
		request.getSession().setAttribute("postid", postid);
		request.getRequestDispatcher("PostViewServlet").forward(request, response);
//		response.sendRedirect("Post");
	}
	
	private void createComment(HttpServletRequest request){		
		Comment comment = new Comment();
		
		// Content
		String content = request.getParameter("comment");
		if(content != null && !(content.isEmpty())){
			comment.setContent(content);
		}		
		System.out.println("HERP");
		User user = (User)request.getSession().getAttribute("curUser");
//		User user = userEJB.getLoggedInUser("email", "password");
		System.out.println(user);
		System.out.println("DERP");
		if(user == null){
			user = userManager.getLoggedInUser("anon", "nona");
			if(user == null){
				System.out.println("LURK");
				user = new User();
				user.setDisplayname("Anonymous");
				user.setEmail("anon");
				user.setPassword("nona");
				user.setRole("anonymous");
				System.out.println("JERK");
			}
		}
		
		comment.setPoster(user);
		
		// Post
		int postId = Integer.parseInt(request.getParameter("post"));
		Post post = postManager.getPost(postId);
		comment.setParentalPost(post);
				
		// Date
		comment.setPostDate(new Timestamp(new Date().getTime()));
		post.addComment(comment);
		postManager.update(post);
	}
	
	private void updateComment(HttpServletRequest request){
		int id = Integer.parseInt(request.getParameter("commentid"));
		Comment comment = commentManager.getComment(id);
		
		if(comment != null){
			String content = request.getParameter("content");
			if(content != null && !(content.isEmpty())){
				comment.setContent(content);
			}
			//Possibly include a last edit date-would need to be added to the entity as well
			commentManager.update(comment);
		}
	}
	
	private void deleteComment(HttpServletRequest request){
		String commentId = request.getParameter("commentId");
		System.out.print("Delete comment id: " + commentId);
		int id = Integer.parseInt(commentId);
		Comment comment = commentManager.getComment(id);
		
		commentManager.delete(id);
	}

}
