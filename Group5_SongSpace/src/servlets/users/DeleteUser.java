package servlets.users;

import java.io.IOException;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.HttpMethodConstraint;
import javax.servlet.annotation.ServletSecurity;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import managers.UserManager;

/**
 * Servlet implementation class DeleteUser
 */
@WebServlet("/DeleteUser")
@ServletSecurity(httpMethodConstraints = {
@HttpMethodConstraint(value = "POST",
				      rolesAllowed = {"admin"})})
public class DeleteUser extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	@EJB
	UserManager um;

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		int delId = Integer.parseInt((String) request.getParameter("delId"));
		um.delete(delId);
		request.getRequestDispatcher("/AllUsers").forward(request, response);
	}
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
		response.sendRedirect("Index");
	}
}
