package servlets.users;

import java.io.IOException;

import javax.annotation.security.DeclareRoles;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.HttpMethodConstraint;
import javax.servlet.annotation.ServletSecurity;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import managers.UserManager;

/**
 * Servlet implementation class AllUsers
 */
@WebServlet("/AllUsers")
@DeclareRoles({"ADMIN"})
@ServletSecurity(httpMethodConstraints = {
		@HttpMethodConstraint(value = "GET",
		rolesAllowed = {"admin"})})
public class AllUsers extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	@EJB
	UserManager um;
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setAttribute("allUsers", um.getUsers());
		request.getRequestDispatcher("allusers.jsp").forward(request, response);
	}

	

}
