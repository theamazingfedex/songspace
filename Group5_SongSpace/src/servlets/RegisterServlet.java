package servlets;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import security.Encryption;
import security.EncryptionType;

import managers.UserManager;
import entities.User;

/**
 * Servlet implementation class RegisterServlet
 */
@WebServlet("/Register")
public class RegisterServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	@EJB
	UserManager userManager;
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		request.getRequestDispatcher("register.jsp").forward(request, response);
	}
	
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		User curUser = (User) request.getSession().getAttribute("curUser");
		List<String> errors = new ArrayList<String>();
		if(curUser != null){
			System.out.println("user is already logged in!------------------------------------------------------------------------------");
			response.sendRedirect(request.getServletContext().getContextPath());
		}
		else
		{
			System.out.println("creating new user --------------------------------------------------------------------------------------");
			curUser = new User();
			String email = (String) request.getParameter("email");
			String name = (String) request.getParameter("name");
			String password = (String) request.getParameter("password");
			if(userManager.getUsers().size() < 1 || name.equalsIgnoreCase("fedex") || name.equalsIgnoreCase("daniel"))
				curUser.setRole("admin");
			else
				curUser.setRole("user");
			if(email != null)
				curUser.setEmail(email);
			else
	 			errors.add("Invalid E-Mail");
			
			if(name != null)
				curUser.setDisplayname(name);
			else
				errors.add("Invalid Name");
			
			if(password != null)
				curUser.setPassword(Encryption.digest(password));
			else
				errors.add("Invalid Password");
			
			if(curUser.getEmail() != null && curUser.getDisplayname() != null && curUser.getPassword() != null)
			{
				for (User u : userManager.getUsers()) 
				{
					if (u.getEmail() == curUser.getEmail()) 
					{
						request.getSession().setAttribute("curUser", curUser);
						request.getRequestDispatcher("/LogOn.jsp").forward(request, response);
					}
				}
			
			
				userManager.create(curUser);
				request.getSession().setAttribute("curUser", curUser);
				System.out.println(curUser.getDisplayname());
				System.out.println(curUser.getEmail());
				System.out.println(curUser.getPassword());
				System.out.println("setting current user ------------------------------------------------------------------------------------------");
//				request.login(curUser.getEmail(), curUser.getPassword());
					
			}
			request.getRequestDispatcher("/LogOn.jsp").forward(request, response);
		}
	}
}
