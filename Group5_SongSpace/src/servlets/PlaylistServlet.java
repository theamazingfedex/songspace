package servlets;

import java.io.IOException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.security.DeclareRoles;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.HttpMethodConstraint;
import javax.servlet.annotation.ServletSecurity;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import entities.Playlist;
import entities.Song;

import managers.PlaylistManager;
import managers.SongManager;

@WebServlet("/PlaylistServlet")
@DeclareRoles({"ADMIN", "USER"})
@ServletSecurity(httpMethodConstraints = {
		@HttpMethodConstraint(value = "POST",
				rolesAllowed = {"ADMIN", "USER"}),
		@HttpMethodConstraint(value = "GET",
		rolesAllowed = {"ADMIN", "USER"})})
public class PlaylistServlet extends HttpServlet{
	
	private static final long serialVersionUID = 1L;
	@EJB PlaylistManager playlistManager;
	@EJB SongManager songManager;
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
		String crudType = request.getParameter("crudType");
		System.out.println(crudType);
		if(crudType.equals("create")){
			request.setAttribute("songs", songManager.getSongs());
			request.getRequestDispatcher("WEB-INF/CreatePlaylist.jsp").forward(request, response);
		}else if(crudType.equals("update")){
			request.setAttribute("songs", songManager.getSongs());			
			request.setAttribute("playlist", playlistManager.getPlaylist(Integer.parseInt(request.getParameter("playlistID"))));
			request.getRequestDispatcher("WEB-INF/UpdatePlaylist.jsp").forward(request, response);
		}else if(crudType.equals("delete")){
			int id = Integer.parseInt(request.getParameter("playlistID"));
			playlistManager.delete(id);
			response.sendRedirect("Index");
		}else if(crudType.equals("preload")){
			request.setAttribute("playlists", playlistManager.getPlaylists());
			request.getRequestDispatcher("WEB-INF/PlaylistsOverview.jsp").forward(request, response);
		}else if(crudType.equals("view")){
			int id = Integer.parseInt(request.getParameter("playlistID"));
			request.setAttribute("playlist", playlistManager.getPlaylist(id));
			request.getRequestDispatcher("WEB-INF/PlaylistView.jsp").forward(request, response);
		}
	}
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
		String crudType = request.getParameter("crudType");
		if(crudType.equals("create")){
			Playlist list = new Playlist();
			
			list.setAddedDate(new Timestamp(new java.util.Date().getTime()));
			
			String desc = request.getParameter("description");
			if(desc != null){
				list.setDescription(desc);
			}
			
			String name = request.getParameter("playlistName");
			if(name != null){
				list.setName(name);
			}
			
			List<Song> songs = new ArrayList<Song>();
			String[] t = request.getParameterValues("songslist");
			if(t != null){
				for(int i = 0; i < t.length; i++){
					int id = Integer.parseInt(t[i]);
					Song song = songManager.getSong(id);
					if(song != null){
						songs.add(song);
					}
				}
				list.setSongs(songs);
			}
			playlistManager.create(list);
		}else if(crudType.equals("update")){
			Playlist list = new Playlist();
			
			int plid = Integer.parseInt(request.getParameter("playlistID"));
			list.setId(plid);
			
			list.setAddedDate(playlistManager.getPlaylist(plid).getAddedDate());
			
			String desc = request.getParameter("description");
			if(desc != null){
				list.setDescription(desc);
			}
			
			String name = request.getParameter("playlistName");
			if(name != null){
				list.setName(name);
			}
			
			List<Song> songs = new ArrayList<Song>();
			String[] t = request.getParameterValues("songslist");
			if(t != null){
				for(int i = 0; i < t.length; i++){
					int sid = Integer.parseInt(t[i]);
					Song song = songManager.getSong(sid);
					if(song != null){
						songs.add(song);
					}
				}
				list.setSongs(songs);
			}
			
			playlistManager.update(list);
		}
		response.sendRedirect("Index");
	}
}
