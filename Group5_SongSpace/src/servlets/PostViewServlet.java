package servlets;

import java.io.IOException;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import managers.PostManager;
import entities.Post;

@WebServlet("/PostViewServlet")
public class PostViewServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@EJB PostManager postManager;
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		dostuff(request, response);
	}
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
		dostuff(request, response);
	}
	
	private void dostuff(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
		String postid = (String) request.getSession().getAttribute("postid");
		int id;
		System.out.println(postid + "POSTID");
		if(postid != null && !(postid.isEmpty())){
			id = Integer.parseInt(postid);
		}else{
			id = Integer.parseInt(request.getQueryString().split("=")[1]);
		}
		Post post = postManager.getPost(id);
		request.setAttribute("post", post);
		request.setAttribute("comments", post.getComments());
		
		request.getRequestDispatcher("/WEB-INF/PostView.jsp").forward(request, response);
	}
}
