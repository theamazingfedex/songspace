package servlets.authentication;

import java.io.IOException;

import javax.annotation.security.DeclareRoles;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.HttpMethodConstraint;
import javax.servlet.annotation.ServletSecurity;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import managers.UserManager;
import security.Encryption;
import entities.User;

/**
 * Servlet implementation class LoginServlet
 */
@WebServlet({"/Login"})
public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	@EJB
	UserManager userManager;
   
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		this.doPost(request, response);
	}

	
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		System.out.println("In login servlet");
		User curUser = (User) request.getSession().getAttribute("curUser");
		if (curUser != null) {
			System.out
					.println("----------------------------------------------");
			System.out.println(curUser.getEmail());
			System.out.println(curUser.getPassword());
			System.out
					.println("----------------------------------------------");
			request.getRequestDispatcher("/Index").forward(request, response);
		}
		else
		{
			
			curUser = new User();
			String email = (String) request.getAttribute("j_username");
			String password = (String) request.getAttribute("j_password");
						System.out.println("email: "+email + "\npassword: "+password +"\n-------------------------------------------------------------");
			request.login(email, password);
			
			curUser = userManager.getLoggedInUser(request.getRemoteUser());
			System.out.println("----------------------------------------------");
			System.out.println(curUser.getEmail());
			System.out.println(curUser.getPassword());
			System.out.println("----------------------------------------------");
			request.getSession().setAttribute("curUser", curUser);
			request.setAttribute("curUser", curUser);
				
			
		}
		request.getRequestDispatcher("/Index").forward(request, response);
	}

}
