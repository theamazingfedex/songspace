package servlets;

import java.io.IOException;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import javax.annotation.security.DeclareRoles;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.HttpMethodConstraint;
import javax.servlet.annotation.ServletSecurity;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import managers.CommentManager;
import managers.PostManager;

import entities.Comment;
import entities.Post;

@WebServlet("/PostManager.do")
@DeclareRoles({"admin"})
@ServletSecurity(httpMethodConstraints = {@HttpMethodConstraint(value = "POST",
																rolesAllowed = {"admin"}),
										  @HttpMethodConstraint(value = "GET",
																rolesAllowed = {"admin"})})
public class PostServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;	
	
	@EJB private PostManager postManager;	
	@EJB private CommentManager commentManager;
		
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
		request.setAttribute("posts", postManager.getPosts());
		request.getRequestDispatcher("PostManagement.jsp").forward(request, response);
	}
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String crud = request.getParameter("crudType");
		
		if(crud != null && !(crud.isEmpty())){
			if(crud.equalsIgnoreCase("create")){
				createPost(request, response);
			}else if(crud.equalsIgnoreCase("update")){
				updatePost(request, response);
			}else if(crud.equalsIgnoreCase("delete")){
				deletePost(request, response);
			}
		}
	}

	private void createPost(HttpServletRequest request, HttpServletResponse response) throws IOException{
		Post post = new Post();

		if(post != null){
			String title = request.getParameter("Title");
			if(title != null && !(title.isEmpty())){
				post.setTitle(title);
			}
			
			String content = request.getParameter("Content");
			if(content != null && !(content.isEmpty())){
				post.setPostContent(content);
			}
			//set author
			post.setAuthor("TestAuthor");
			
			post.setPostDate(new Timestamp(new Date().getTime()));
			
			postManager.create(post);
		}
		
		List<Post> posts = postManager.getPosts();
		request.getSession().setAttribute("posts", posts);
		response.sendRedirect("Index");
	}
	
	private void updatePost(HttpServletRequest request, HttpServletResponse response) throws IOException{
		int id = Integer.parseInt(request.getParameter("postid"));
		Post post = postManager.getPost(id);
		
		if(post != null){
			//edit title
			String title = request.getParameter("Title");
			if(title != null && !(title.isEmpty())){
				post.setTitle(title);
			}
			//edit content
			String content = request.getParameter("Content");
			if(content != null && !(content.isEmpty())){
				post.setPostContent(content);
			}
			
			//edit comments
			String commentIDtemp = request.getParameter("commentid");
			if(commentIDtemp != null && !commentIDtemp.isEmpty()){
				int commentID = Integer.parseInt(commentIDtemp);
				Comment comment = commentManager.getComment(commentID);
				
				String comType = request.getParameter("comType");
				if(comment != null){
					//add comment
					if(comType.equalsIgnoreCase("add")){
						post.addComment(comment);
					//remove comment
					}else if(comType.equalsIgnoreCase("remove")){
						post.removeComment(comment);
					}
				}
			}
			
			postManager.update(post);
		}
		
		List<Post> posts = postManager.getPosts();
		request.getSession().setAttribute("posts", posts);
		response.sendRedirect("PostManagement.jsp");
	}
	
	private void deletePost(HttpServletRequest request, HttpServletResponse response) throws IOException{
		int id = Integer.parseInt(request.getParameter("postid"));
		Post post = postManager.getPost(id);
		
		if(post != null){
			postManager.delete(id);
		}
		
		List<Post> posts = postManager.getPosts();
		request.getSession().setAttribute("posts", posts);
		response.sendRedirect("PostManagement.jsp");
	}
}
