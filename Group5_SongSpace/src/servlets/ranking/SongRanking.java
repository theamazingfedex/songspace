package servlets.ranking;

import java.io.IOException;

import javax.annotation.security.DeclareRoles;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.HttpMethodConstraint;
import javax.servlet.annotation.ServletSecurity;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import entities.Song;
import managers.SongManager;


@WebServlet("/SongRanking")
@DeclareRoles({"ADMIN", "USER"})
@ServletSecurity(httpMethodConstraints = {
		@HttpMethodConstraint(value = "POST",
				rolesAllowed = {"ADMIN", "USER"}),
		@HttpMethodConstraint(value = "GET",
		rolesAllowed = {"ADMIN", "USER"})})
public class SongRanking extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    @EJB
    SongManager songManager;
    
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		int curSongId = Integer.parseInt((String) request.getParameter("curSongId"));
		Song curSong = songManager.getSong(curSongId);
		int rating = 0;
		String strRating = request.getParameter("curRating");
		if (strRating != null)
			rating = Integer.parseInt(strRating);
		curSong.setRanking(curSong.getRanking() + rating);
		songManager.update(curSong);
		request.setAttribute("songs", songManager.getSongs());
		request.getRequestDispatcher("WEB-INF/SongsOverview.jsp").forward(request, response);
	}
}
