package servlets.ranking;

import java.io.IOException;

import javax.annotation.security.DeclareRoles;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.HttpMethodConstraint;
import javax.servlet.annotation.ServletSecurity;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import managers.PlaylistManager;
import entities.Playlist;

/**
 * Servlet implementation class PlaylistRanking
 */
@WebServlet("/PlaylistRanking")
@DeclareRoles({"ADMIN", "USER"})
@ServletSecurity(httpMethodConstraints = {
		@HttpMethodConstraint(value = "POST",
				rolesAllowed = {"ADMIN", "USER"}),
		@HttpMethodConstraint(value = "GET",
		rolesAllowed = {"ADMIN", "USER"})})
public class PlaylistRanking extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    @EJB
    PlaylistManager playlistManager;
    
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		int curPlaylistId = Integer.parseInt((String) request.getParameter("curPlaylistId"));
		Playlist curPlaylist = playlistManager.getPlaylist(curPlaylistId);
		String strRating = request.getParameter("curRating");
		int rating = 0;
		if (strRating != null)
			rating = Integer.parseInt(strRating);
		curPlaylist.setRanking(curPlaylist.getRanking() + rating);
		playlistManager.update(curPlaylist);
		request.setAttribute("playlists", playlistManager.getPlaylists());
		request.getRequestDispatcher("WEB-INF/PlaylistsOverview.jsp").forward(request, response);
	}

}
