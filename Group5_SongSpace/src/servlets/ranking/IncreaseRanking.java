package servlets.ranking;

import java.io.IOException;
import java.util.List;

import javax.annotation.security.DeclareRoles;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.HttpMethodConstraint;
import javax.servlet.annotation.ServletSecurity;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import managers.SongManager;
import managers.UserManager;
import entities.Playlist;
import entities.User;

/**
 * Servlet implementation class IncreaseRanking
 */
@WebServlet({"/IncreaseRanking", "/Test"})
@DeclareRoles({"ADMIN", "USER"})
@ServletSecurity(httpMethodConstraints = {
		@HttpMethodConstraint(value = "POST",
				rolesAllowed = {"ADMIN", "USER"}),
		@HttpMethodConstraint(value = "GET",
		rolesAllowed = {"ADMIN", "USER"})})
public class IncreaseRanking extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	@EJB
	SongManager sm;
	@EJB
    UserManager um;
    
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String pathInfo = request.getPathInfo();
		int playlistId = Integer.parseInt(pathInfo.substring(pathInfo.lastIndexOf('/')+1));
		int value = Integer.parseInt((String) request.getAttribute("playlistRating"));
		User u = um.getLoggedInUser(request.getRemoteUser());
		List<Playlist> playlists = u.getPlaylists();
		for (Playlist p : playlists)
		{
			if (p.getId() == playlistId)
				p.setRanking(p.getRanking()+1);
		}
		request.setAttribute("uname", request.getRemoteUser());
		request.setAttribute("curUser", u);
		request.setAttribute("test", "ranking has been increased");
		request.getRequestDispatcher("/Index").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		this.doGet(request, response);
	}

}
