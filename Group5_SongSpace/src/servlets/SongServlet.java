package servlets;


import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.annotation.security.DeclareRoles;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.HttpMethodConstraint;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.ServletSecurity;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

import entities.Song;

import managers.SongManager;

@MultipartConfig
@WebServlet("/SongServlet")
@DeclareRoles({"ADMIN", "USER"})
@ServletSecurity(httpMethodConstraints = {
		@HttpMethodConstraint(value = "POST",
				rolesAllowed = {"ADMIN", "USER"}),
		@HttpMethodConstraint(value = "GET",
		rolesAllowed = {"ADMIN", "USER"})})
public class SongServlet extends HttpServlet {
	@EJB SongManager sm;
       

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String crudType = request.getParameter("crudType");
		if(crudType.equals("create")){
			request.getRequestDispatcher("WEB-INF/UploadSong.jsp").forward(request, response);
		}else if(crudType.equals("update")){
			request.setAttribute("song", sm.getSong(Integer.parseInt(request.getParameter("songID"))));
			request.getRequestDispatcher("WEB-INF/UpdateSong.jsp").forward(request, response);
		}else if(crudType.equals("delete")){
			int id = Integer.parseInt(request.getParameter("songID"));
			sm.delete(id);
			request.setAttribute("songs", sm.getSongs());
			request.getRequestDispatcher("WEB-INF/SongsOverview.jsp").forward(request, response);
		}else if(crudType.equals("preload")){
			request.setAttribute("songs", sm.getSongs());
			request.getRequestDispatcher("WEB-INF/SongsOverview.jsp").forward(request, response);
		}else if(crudType.equals("view")){
			int id = Integer.parseInt(request.getParameter("songID"));
			Song tempSong = sm.getSong(id);
			request.setAttribute("song", tempSong);
			request.getRequestDispatcher("WEB-INF/ViewSong.jsp").forward(request, response);
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String crudType = request.getParameter("crudType");
		if(crudType == null){
			Part p = request.getPart("crudType");
			crudType = p.toString();
		}
		System.out.println(crudType);
		if(crudType.equals("create")){
			Song song = new Song();
			
			song.setAddedDate(new Timestamp(new java.util.Date().getTime()));
			song.setRanking(3);
			
			String sf = request.getParameter("fname");
			System.out.println(sf);
			Part songFile = request.getPart("fname");
			if(songFile != null){
				System.out.println("HEPR");
				song.setData(getBytes(songFile.getInputStream()));
				song.setDataType(songFile.getContentType());
			}else{
				System.out.println("DERP");
			}
			
			String artist = request.getParameter("artist");
			if(artist == null){
				Part artistPart = request.getPart("artist");
				if(artistPart != null){
					artist = artistPart.toString();
				}
			}
			System.out.println(artist);
			song.setArtist(artist);				
			
			String description = request.getParameter("description");
			if(description == null){
				Part desPart = request.getPart("description");
				if(desPart != null){
					description = desPart.toString();
				}
			}
			System.out.println(description);
			song.setDescription(description);
			
			String songName = request.getParameter("songName");
			if(songName == null){
				Part songNamePart = request.getPart("songName");
				if(songNamePart != null){
					songName = songNamePart.toString();
				}
			}
			System.out.println(songName);
			song.setName(songName);
			
			sm.create(song);
			
		}else if(crudType.equals("update")){
			Song song = new Song();
			String songID = request.getParameter("songID");
			int sid = 0;
			if(songID != null){
				sid = Integer.parseInt(songID);
				song.setId(sid);
			}
			
			String name = request.getParameter("name");
			if(name != null){
				song.setName(name);
			}
			
			String description = request.getParameter("description");
			if(description != null){
				song.setDescription(description);
			}
			
			String artist = request.getParameter("artist");
			if(artist != null){
				song.setArtist(artist);
			}
			
			song.setAddedDate(sm.getSong(sid).getAddedDate());
			song.setRanking(sm.getSong(sid).getRanking());
		}
		response.sendRedirect("Index");
	}
	
	private byte[] getBytes(InputStream instream) {		
		ByteArrayOutputStream buffer = new ByteArrayOutputStream();

		int nRead;
		byte[] data = new byte[16384];

		try {
			while ((nRead = instream.read(data, 0, data.length)) != -1) {
			buffer.write(data, 0, nRead);
			}
		} catch (IOException e1) {
			e1.printStackTrace();
		}

		try {
			buffer.flush();
		} catch (IOException e) {
			e.printStackTrace();
		}

		return buffer.toByteArray();
	}
	
//	private File getFile(byte[] data) throws IOException, ClassNotFoundException{
//		ByteArrayInputStream bis = new ByteArrayInputStream(data);
//		ObjectInputStream ois = new ObjectInputStream(bis);
////		File tempFile = (File) ois.readObject();
////		bis.close();
////		ois.close();
//		//return tempFile;
//		return null;
//	}
}
