package managers;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.transaction.Transaction;

import security.Encryption;
import entities.User;

@Stateless
@LocalBean
public class UserManager {
	@PersistenceContext
	EntityManager em;
	
	public User create(User p){
		em.persist(p);
		em.flush();
		return p;
	}
	
	public User update(User p){
		em.merge(p);
		em.flush();
		return p;
	}
	
	public void delete(int id){
		User list = getUser(id);
		if(list != null){
			em.remove(list);
			em.flush();
		}
	}
	
	public User getUser(int id){
		return em.getReference(User.class, id);
	}
	
	public List<User> getUsers(){
		Query getUsers = em.createQuery("SELECT p FROM User p", User.class);
		return (List<User>) getUsers.getResultList();
	}

	public User getLoggedInUser(String email, String password){
		User tempUser = null;
		TypedQuery<User> getUsers = em.createQuery("SELECT u FROM User u", User.class);
		for(User u : getUsers.getResultList()){
			if (u.getEmail().equals(email) && u.getPassword().equals(Encryption.digest(password)));
				tempUser = u;
		}
		return tempUser;
	}
	public User getLoggedInUser(String email){
		User tempUser = null;
		TypedQuery<User> getUsers = em.createQuery("SELECT u FROM User u WHERE u.email= :email", User.class);
		getUsers.setParameter("email", email);
		tempUser = getUsers.getSingleResult();
		return tempUser;
	}
}