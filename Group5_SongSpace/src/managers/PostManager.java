package managers;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import entities.Post;

@Stateless
@LocalBean
public class PostManager {
	@PersistenceContext
	EntityManager em;
	
	public Post create(Post p){
		em.persist(p);
		em.flush();
		return p;
	}
	
	public Post update(Post p){
		em.merge(p);
		em.flush();
		return p;
	}
	
	public void delete(int id){
		Post list = getPost(id);
		if(list != null){
			em.remove(list);
			em.flush();
		}
	}
	
	public Post getPost(int id){
		return em.getReference(Post.class, id);
	}
	
	public List<Post> getPosts(){
		Query getPosts = em.createQuery("SELECT p FROM Post p", Post.class);
		return (List<Post>) getPosts.getResultList();
	}
}