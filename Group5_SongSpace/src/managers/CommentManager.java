package managers;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import entities.Comment;

@Stateless
@LocalBean
public class CommentManager {
	@PersistenceContext
	EntityManager em;
	
	public Comment create(Comment p){
		em.persist(p);
		em.flush();
		return p;
	}
	
	public Comment update(Comment p){
		em.merge(p);
		em.flush();
		return p;
	}
	
	public void delete(int id){
		Comment list = getComment(id);
		if(list != null){
			em.remove(list);
			em.flush();
		}
	}
	
	public Comment getComment(int id){
		return em.getReference(Comment.class, id);
	}
	
	public List<Comment> getComments(){
		Query getComments = em.createQuery("SELECT p FROM Comment p", Comment.class);
		return (List<Comment>) getComments.getResultList();
	}
}