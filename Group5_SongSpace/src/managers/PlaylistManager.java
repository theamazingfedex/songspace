package managers;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import entities.Playlist;

@Stateless
@LocalBean
public class PlaylistManager {
	@PersistenceContext
	EntityManager em;
	
	public Playlist create(Playlist p){
		em.persist(p);
		em.flush();
		return p;
	}
	
	public Playlist update(Playlist p){
		em.merge(p);
		em.flush();
		return p;
	}
	
	public void delete(int id){
		Playlist list = getPlaylist(id);
		if(list != null){
			em.remove(list);
			em.flush();
		}
	}
	
	public Playlist getPlaylist(int id){
		return em.getReference(Playlist.class, id);
	}
	
	public List<Playlist> getPlaylists(){
		Query getRecipes = em.createQuery("SELECT p FROM Playlist p", Playlist.class);
		return (List<Playlist>) getRecipes.getResultList();
	}
}
