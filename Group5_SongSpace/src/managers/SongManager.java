package managers;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import entities.Song;

@Stateless
@LocalBean
public class SongManager {
	@PersistenceContext
	EntityManager em;
	
	public Song create(Song p){
		em.persist(p);
		em.flush();
		return p;
	}
	
	public Song update(Song p){
		em.merge(p);
		em.flush();
		return p;
	}
	
	public void delete(int id){
		Song list = getSong(id);
		if(list != null){
			em.remove(list);
			em.flush();
		}
	}
	
	public Song getSong(int id){
		return em.getReference(Song.class, id);
	}
	
	public List<Song> getSongs(){
		Query getSongs = em.createQuery("SELECT p FROM Song p", Song.class);
		return (List<Song>) getSongs.getResultList();
	}
}