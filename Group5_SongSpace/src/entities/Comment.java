package entities;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.JoinColumn;

@Entity
public class Comment {
	
	@Id
	@GeneratedValue
	private int commentId;
	
	@Lob
	@Column(nullable=false)
	private String content;
	
	@Column(nullable=false, updatable=false)
	@Temporal(TemporalType.TIMESTAMP)
	private Date postDate;
	
	@ManyToOne
	private Post parentalPost;
	
	@ManyToOne
	@JoinColumn(name = "posterId", referencedColumnName = "userId")
	private User poster;

	
	public int getCommentId(){
		return this.commentId;
	}
	public void setCommentId(int id){
		this.commentId = id;
	}
	
	public String getContent(){
		return this.content;
	}
	public void setContent(String content){
		this.content = content;
	}
	
	public User getPoster(){
		return poster;
	}
	public void setPoster(User poster){
		this.poster = poster;
	}
	
	public Date getPostDate(){
		return this.postDate;
	}
	public void setPostDate(Date date){
		this.postDate = date;
	}

	public Post getParentalPost(){
		return this.parentalPost;
	}
	public void setParentalPost(Post post){
		this.parentalPost = post;
	}
}
