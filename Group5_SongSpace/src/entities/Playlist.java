package entities;

import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "PLAYLISTS")
public class Playlist {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "playlist_id")
	private int id;
	
	@Column(name = "playlist_name")
	private String name;
	
	@ManyToMany
	@JoinTable(name="PLAYLIST_SONGS",
			   joinColumns= @JoinColumn(name="playlist_id"),
			   inverseJoinColumns= @JoinColumn(name="song_id"))
	private List<Song> songs;
	
	@Column(nullable = false, updatable = false)
	@Temporal(TemporalType.TIMESTAMP)
	private Date addedDate;

	private String description;
	
	@Column(nullable=false)
	private int ranking = 0;
	
	public int getRanking(){
		return this.ranking;
	}
	
	public void setRanking(int rank){
		this.ranking = rank;
	}
	
	public String getDescription() {
		return this.description;
	}
	
	public String setDescription(String desc) {
		this.description = desc;
		return this.description;
	}
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<Song> getSongs() {
		return songs;
	}

	public void setSongs(List<Song> songs) {
		this.songs = songs;
	}

	public Date getAddedDate() {
		return addedDate;
	}

	public void setAddedDate(Date addedDate) {
		this.addedDate = addedDate;
	}
	
	
	
}
